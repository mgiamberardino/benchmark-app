# Kotlin + Vert.x Benchmark app

## Running

The most convenient way to run the project during development is as follows:

```
./gradlew run
```

That way, any changes to the classes will be picked up and re-deployed automatically.

## Deployment
### Manual

Generate a shadow jar as follows:

```
./gradlew shadowJar
```

The jar file can now be retrieved from `./build/libs/benchmark-app-shadow.jar` and deployed onto your preferred platform.
Run it as follows:

```
java -jar benchmark-app-shadow.jar
```

You can include any extra Vert.x configuration like -instances to tune the amount of event loops to deploy

### Docker
Build the image running
```
docker build . -t benchmark-app-kotlin
```

And then run it 
```
docker run -p 80:80 benchmark-app-kotlin
```

## JMH Benchmarks
To run JMH benchmarks run the following command
```
./gradlew jmh
```