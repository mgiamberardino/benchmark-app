package com.sparkdigital.benchmark

import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx

fun main(args: Array<String>) {
    val vertx = Vertx.vertx()
    val config = DeploymentOptions().setInstances(4).setHa(true)
    vertx.deployVerticle(BenchmarkApiVerticle().javaClass.name, config) { ar ->
        if (ar.succeeded()) {
            println("Application started")
        } else {
            println("Could not start application")
            ar.cause().printStackTrace()
        }
    }
}
