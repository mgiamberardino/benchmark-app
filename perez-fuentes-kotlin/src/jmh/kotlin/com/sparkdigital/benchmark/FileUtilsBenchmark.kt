package com.sparkdigital.benchmark

import org.openjdk.jmh.annotations.*
import java.nio.file.Files

@State(Scope.Benchmark)
@Fork(3)
@Warmup(iterations = 5)
@Measurement(iterations = 5)
open class FileUtilsBenchmark {

    @Setup
    fun setUp(): Unit {
    }

    @Benchmark
    fun countVowelsBenchmark(): Int = FileUtils.countVowelsInFile("support/ram_test.txt")

    @Benchmark
    fun createTempFileBenchmark() = Files.delete(FileUtils.copyToTempFile("support/disk_test.csv").path)
}
