"""
Contains the cpu bound resource
"""
from hashlib import sha512
import json
import falcon


class CpuResource:
    """
    Has a method for handling cpu bound get requests
    """
    def on_get(self, req: falcon.Request, resp: falcon.Response):
        """
        Handles GET requests
        """
        hash_me = sha512(b'Sparkers doing some benchmarking').digest()
        for i in range(0, 256):
            hash_me = sha512(hash_me).digest()

        resp.status = falcon.HTTP_OK
        resp.media = json.dumps({'Hashed': str(hash_me)})
