#pylint: disable=C0111
import unittest
import requests

from threading import Thread
from wsgiref.simple_server import make_server

from main import APP

class TestUM(unittest.TestCase):
    host = '127.0.0.1'
    port = 8000

    def setUp(self):
        self.httpd = make_server(self.host, self.port, APP)
        self.server_thread = Thread(target=self.httpd.serve_forever, daemon=True)
        print("Starting Edwin's benchmark service")
        self.server_thread.start()

    def tearDown(self):
        print("Stopping Edwin's benchmark service")
        self.httpd.shutdown()
        self.server_thread.join()

    def test_cpu_endpoint(self):
        result = requests.get(f"http://{self.host}:{self.port}/cpu")
        self.assertEqual(result.status_code, 200)

    def test_ram_endpoint(self):
        result = requests.get(f"http://{self.host}:{self.port}/ram")
        self.assertEqual(result.status_code, 200)

    def test_disk_endpoint(self):
        result = requests.get(f"http://{self.host}:{self.port}/disk")
        self.assertEqual(result.status_code, 200)

    def test_throughput_endpoint(self):
        result = requests.get(f"http://{self.host}:{self.port}/throughput")
        self.assertEqual(result.status_code, 200)


if __name__ == '__main__':
    unittest.main()
