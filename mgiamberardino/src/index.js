const cp = require('child_process');
const os = require('os');

class Master {
  constructor(){
    process.title = 'Master Process';
    this.fork();
  }
  fork(){
    os.cpus().forEach( (cpu, idx) => {
      console.log(`Starting Worker ${idx} `)
      cp.fork('./raw-server', {env: {id: idx, debugPort: 8500 + idx, port: 8000 + idx}});
    });
  }
}

new Master();
