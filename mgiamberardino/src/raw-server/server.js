var http = require("http");

class Server {
  constructor(){
    console.log(`Process id: ${process.pid}`);
    this.server = http.createServer(this.handleRequest.bind(this));
    this.urls = {};
  }
  async handleRequest(request, response){
    let headers = {
      'Content-Type': 'application/json'
    };
    if (this.urls[request.url]){
      response.writeHead(200, headers);
      response.end(JSON.stringify(await this.urls[request.url](request)));
    }
    return this.notFoundHandler(response);
  }
  notFoundHandler(response){
    response.writeHead(404);
    response.end();
  }
  register(routes){
    routes.forEach(route => this.urls[route.path] = route.handler);
    return this;
  }
  start(){
    this.server.listen(process.env.port);
    return this;
  }
}

module.exports = Server;
