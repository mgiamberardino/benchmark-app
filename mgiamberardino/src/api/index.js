module.exports = {
  routes: [
    {
      handler: require('./throughput'),
      path: '/throughput'
    },
    {
      handler: require('./cpu'),
      path: '/cpu'
    },
    {
      handler: require('./ram'),
      path: '/ram'
    },
    {
      handler: require('./disk'),
      path: '/disk'
    }
  ]
};
