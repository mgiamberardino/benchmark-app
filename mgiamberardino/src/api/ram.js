const fs = require('fs');
const vowels = ['a', 'e', 'i', 'o', 'u'];

const countFileVowels = async (file) => {
  return new Promise((resolve, reject) => {
    const src = fs.createReadStream(file); //,{ highWaterMark: 1 }
    let count = 0;
    src.on('data', (chunk) => {
      count += chunk.toString().replace(/[^a^e^i^o^u]/g, '').length;
    });
    src.on('end', () => {
      resolve(count);
    });
  });
};

module.exports = (request) => {
  return countFileVowels('/support/ram_test.txt')
    .then(count => {
      return {
        n_vowels: count
      };
    });
};
