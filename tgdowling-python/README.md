# Tomas Gonzalez Dowling submit for the benchmarking contest

This service was developed using japronto micro-framework and Python.

The port configured for the docker container is 80.
Following endpoints should be available when docker container is running:

http://localhost/ram
http://localhost/cpu
http://localhost/disk
http://localhost/throughput


To test it you can run the following commands:

docker build . -t tomas-gonzalez-dowling

docker run -p 80:80 tomas-gonzalez-dowling

